# Listening and Active Communication

## Question 1
### What are the steps/strategies to do Active Listening?
- Refrain from letting your own thoughts divert you.
- Avoid interrupting the other person.
- After they're done, let them finish before answering.
- employ door openers These are expressions of interest that allow the other person to continue speaking while you use body language to indicate that you're paying attention.
- concentrate instead on the speaker and subject
- When appropriate, take notes during crucial discussions. To ensure that you both understand one other, paraphrase what others have said.

## Question 2
### According to Fisher's model, what are the key points of Reflective Listening?
Fisher claims that decision-making can be repetitious and inconsistent at times. Depending on the circumstances, one can examine the various routes that interactants take to reach a choice. Fisher's model's four phases help us comprehend how a decision moves through a group.

- **Orientation:** Getting to know one another is the first step.
- **Conflict:** After examining the issue, each party makes an effort to present a viewpoint that leads to a solution.
- **Emergence:** At this point, people are quieter yet there is still an increase in uncertainty.
- **Reinforcement:** Using a variety of their own perspectives, the members attempt to observe their final judgement. It strengthens the group's unity.


## Question 3
### What are the obstacles in your listening process?
- Overwhelming information
- Prejudice or bias
- Speaking and thinking rates
- Distractions from the outside and inside.


## Question 4
### What can you do to improve your listening?
- Think about making eye contact.
- Be vigilant, but not overly so.
- Pay attention to nonverbal cues like tone and body language.
- Visualise what the speaker is saying in your mind.
- Feel the same as the speaker
- Give a critical assessment.
- Have an open mind.

## Question 5
### When do you switch to Passive communication style in your day to day life?
- You temporarily avoid or put off conflict.
- Short-term anxiety is reduced by you.
- You get a lot of accolades for your selflessness.
- Others try to watch out for you.
- You are rarely blamed if things go wrong because you have not put yourself out
- There or taken control of the situation

## Question 6
### When do you switch into Aggressive communication styles in your day to day life?
- Not everything is going as planned.
- unable to comprehend why someone else is cruel.
- when someone is held accountable for something they did not do or do not know about.
- to manipulate and take control of a situation.

## Question 7
### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- While requesting that others refrain from interfering with work when a deadline is imminent.
- requesting that someone vacate the space.
- enquiring as to why a person is late.
- gaining the upper hand through respect.


## Question 8
### How can you make your communication assertive?
- Be Truthful About Your Dislikes
- Avoid Judging or Exaggerating
- Employ "I" statements (e.g., "I'd like a little quiet").
- Combine Everything. (For example: You reassure me when you arrive on time.)
- Enumerate actions, outcomes, and emotions. (For example: I believe you are doing this on purpose when you don't work, and that can also result in firing.)
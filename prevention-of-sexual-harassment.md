# Prevention of Sexual Harassment

## Definition: sexual harassment
Any form of unwanted verbal, non-verbal or physical conduct of a sexual nature occurs, with the purpose or effect of violating the dignity of a person, in particular when creating an intimidating, hostile, degrading, humiliating or offensive environment.

## Kinds of behaviour cause sexual harassment

### 1. Unwanted Sexual Comments or Advances:
- Inappropriate remarks : "Inappropriate remarks" refer to comments or statements that are considered unsuitable, offensive, or improper in a given context.

-   jokes: "jokes" refer to comments, remarks, or statements of a humorous nature that involve sexual content and are inappropriate or unwelcome.

-   Unwanted advances: Unwanted advances in a sexual context refer to any unwelcome or non-consensual actions, gestures, or behaviors of a sexual nature directed toward another person.

-   requests for sexual favors, or propositions : Requests for sexual favors in the context of sexual harassment refer to unwelcome and non-consensual demands or propositions for sexual activities made by one person to another.

### 2. Non-Consensual Touching:
-   Touching : In the context of sexual harassment, touching refers to any physical contact of a sexual nature that is unwelcome, non-consensual, and creates discomfort or distress for the recipient.
    some examples of inappropriate touching in the context of sexual harassment:
    Groping, Forced Kisses, Unwanted Hugging or Touching, Sexual Assault.

-   groping, or any physical contact of a sexual nature without consent : Groping and any other form of physical contact of a sexual nature without consent are clear examples of sexual harassment. This behavior involves touching someone's body inappropriately and is a serious violation of personal boundaries. Unwanted physical contact can create a hostile and intimidating environment for the person on the receiving end.

### 3. Sexual Coercion:
-   Sexual coercion refers to the act of using pressure, force, manipulation, or authority to compel someone to engage in sexual activities against their will. In the context of sexual harassment, sexual coercion is a serious violation that can create a hostile and intimidating environment for the victim.

-   Using power : Using power in sexual harassment involves leveraging one's position, authority, or influence to engage in unwelcome and inappropriate sexual behaviors, creating a hostile or intimidating environment for the victim
    pressure to force someone into engaging in sexual activities against their will.

### 4. Display of Inappropriate Material:
-   Display of inappropriate material in the context of sexual harassment refers to the showing or sharing of explicit, offensive, or sexually suggestive materials that can create a hostile or uncomfortable environment for others. This behavior can occur in various settings, including workplaces, educational institutions, or social environments

-   Showing or sharing explicit or sexually suggestive materials, such as photos, videos, or messages.

### 5. Cyber Harassment:
-   Cyber harassment in the context of sexual harassment refers to the use of digital platforms, communication tools, or online spaces to engage in unwelcome and inappropriate behavior of a sexual nature. This form of harassment can occur through various online channels and may involve digital communication, sharing explicit content, or online stalking.

-   Sending unsolicited explicit messages or images through digital platforms.

-   Engaging in online stalking or harassment of a sexual nature.

### 6. Sexualized Bullying:
-   Sexualized bullying refers to the use of sexual elements or themes in a bullying context, where individuals are targeted based on their gender, appearance, or perceived sexual orientation.

-   Using sexuality as a means to bully, humiliate, or intimidate someone.

### 7. Unwanted Sexual Attention:
-   Unwanted sexual attention in the context of sexual harassment refers to persistent and unwelcome behaviors of a sexual nature directed towards an individual. This attention may take various forms and can create a hostile or uncomfortable environment for the person on the receiving end.

-   Persistently staring, leering, or making sexually suggestive gestures.

### 8. Sexual Discrimination:
-   It appears there might be a confusion in the terms used. Sexual discrimination and sexual harassment are related concepts, but they refer to distinct aspects of inappropriate behavior based on gender or sex. 

-   Treating someone unfairly or creating a hostile environment based on their gender or sexual orientation.

### 9. Retaliation for Rejection:
-   Retaliation for rejection in the context of sexual harassment refers to adverse actions taken against an individual who has refused or rejected unwelcome advances, requests for sexual favors, or any other form of inappropriate behavior. Retaliation can occur when the harasser responds negatively to the rejection by punishing, mistreating, or taking retaliatory actions against the person who rejected them. 

-   Punishing or retaliating against someone for rejecting sexual advances.

### 10. Creating a Hostile Work Environment:
-   Creating a hostile work environment in the context of sexual harassment involves behaviors or actions that make the workplace intimidating, offensive, or uncomfortable due to unwelcome and inappropriate conduct of a sexual nature. It can affect the well-being, productivity, and job satisfaction of individuals who experience or witness such behavior.

-   Cultivating an environment where sexually inappropriate behavior is tolerated or ignored.



## I can provide general guidance on what individuals can do if they face or witness inappropriate behavior:

1.  **Assess the situation**: Before taking any action, assess the situation to determine the severity of the behavior and whether it poses an immediate threat.

2. **Ensure personal safety**: Prioritize your safety and the safety of others. If the situation is dangerous, remove yourself and others from harm's way.

3. **Document the incident**: If it's safe to do so, document the incident by taking notes, photos, or videos. This can serve as evidence if further action is required.

4. **Report to authorities**: If the behavior is illegal or poses a threat, report it to the appropriate authorities, such as the police. Provide them with any evidence you have collected.

5. **Speak up**: If you feel comfortable and safe, confront the person engaging in inappropriate behavior and express your concerns. Sometimes, people may not be aware of the impact of their actions.

6. **Seek support**: Talk to friends, family, or colleagues about the incident. Seeking support can help you cope with the experience and may provide valuable insights on how to address the situation.

7. **Report to relevant authorities or organizations**: If the incident occurs in a workplace, school, or other organized setting, report it to the relevant authorities or human resources department. Many organizations have protocols for handling such issues.

8. **Use available resources**: Depending on the context, there may be hotlines, support groups, or counseling services available to assist individuals who have experienced or witnessed inappropriate behavior.

9. **Educate and raise awareness**: Advocate for awareness and education on appropriate behavior in your community, workplace, or educational institution. Encourage a culture of respect and inclusion.
## Question 1
### What is the Feynman Technique? Explain in 1 line.
**Answer :**  The Feynman Technique is a learning method that involves explaining a concept in simple terms as if you were teaching it to someone else.

## Question 2
### In this video, what was the most interesting story or idea for you?(Learning How to Learn TED talk by Barbara Oakley)
**Answer :** 
Barbara Oakley, in her TED talk, delves into her personal journey of grappling with math and science, only to discover effective learning strategies later on. She sheds light on two distinct modes of thinking, namely focused and diffuse, and elucidates their influence on the learning process. By drawing upon examples from the realms of artistry and invention, Oakley explores the ways in which individuals can overcome procrastination. Additionally, she introduces the Pomodoro technique as a means to enhance focused work and underscores the significance of testing, practice, and recall in the pursuit of mastery. Oakley emphasizes that comprehension alone is insufficient for achieving expertise.


## Question 3
### What are active and diffused modes of thinking?
**Answer :**

**Active Mode of Thinking:**

-   **1. Definition:** Active thinking involves focused and concentrated attention on a specific task or problem.

-   **2. Characteristics:** It is a state of deep concentration, where the mind is actively engaged in processing information, analyzing details, and solving problems.

**Diffused Mode of Thinking:**

-   **1. Definition:** Diffused thinking is a more relaxed and unfocused state of mind.

-   **2. Characteristics:** In this mode, the mind is not fixated on a particular problem, allowing thoughts to wander and connections to be made more freely across different areas of the brain.


## Question 4
### According to the video, what are the steps to take when approaching a new topic? Only mention the points.
**Answer :**

-   Deconstruct the skill
-   Learn enough to self-correct.
-   Remove barriers to practice(distractions).
-   Practice for at least 20 hours.


## Question 5
### What are some of the actions you can take going forward to improve your learning process?
**Answer :**

- Embrace Deep Work
- Preserve Attention
- Use Productivity Tools
- Make Learning Enjoyable
- Master a Few Skills
- Evaluate Yourself Regularly
- Continuous Improvement
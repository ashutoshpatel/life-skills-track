# OSI (Model) Layer

## Table of Contents
- [OSI (Model) Layer](#osi-model-layer)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
- [OSI Model Layers](#osi-model-layers)
  - [1. Application Layer](#1-application-layer)
  - [2. Presentation Layer](#2-presentation-layer)
  - [3. Session Layer](#3-session-layer)
  - [4. Transport Layer](#4-transport-layer)
  - [5. Network Layer](#5-network-layer)
  - [6. Data Link Layer](#6-data-link-layer)
  - [7. Physical Layer](#7-physical-layer)
  - [Reference](#reference)

## Introduction
The full name of the OSI model is the Open Systems Interconnection Model.

OSI is defined and used to understand how data is transferred from one computer to another in a computer network.

Why do we use the OSI model? Let's assume one computer is based on Microsoft Windows and another computer is based on macOS; then, how can these computers communicate with each other?

To enable successful communication between computers, the OSI model, which stands for Open Systems Interconnection Model, was introduced by the International Organization for Standardization (ISO).

The OSI model consists of 7 layers:
1. Application Layer
2. Presentation Layer
3. Session Layer
4. Transport Layer
5. Network Layer
6. Data Link Layer
7. Physical Layer

# OSI Model Layers

## 1. Application Layer
The Application Layer is used by network applications, meaning computer applications that use the internet, such as Google Chrome, Firefox, etc. Web browsers use application layer protocols like HTTP and HTTPS for web surfing.

All network applications depend on application layer protocols to run. There are multiple application layer protocols that serve various functions. For file transfer, we use the FTP protocol; for web surfing, we use HTTP/HTTPS protocols, and for emails, we use the SMTP protocol.

The application layer provides services for network applications with the help of protocols to perform user activities.

## 2. Presentation Layer
The Presentation Layer receives data from the Application Layer. This data is in the form of characters and numbers, and the Presentation Layer converts these characters and numbers into a machine-understandable binary format.

For example, the Presentation Layer performs functions like the conversion of ASCII to abstract code. It also reduces the number of bits used to represent the original data through a process called data compression. To maintain data integrity before transmission, data is encrypted, enhancing the security of sensitive information. Encryption occurs at the sender side, and decryption occurs at the receiver side.

Therefore, the Presentation Layer performs three basic functions: translation, compression, and encryption-decryption.

## 3. Session Layer
The Session Layer assists in setting up and managing connections, enabling the sending and receiving of data, followed by the termination of connections or sessions. The Session Layer has its own helpers called APIs (Application Programming Interfaces), which aid in session management, authentication, and authorization.

For example, planning a party involves hiring helpers to ensure that each activity runs smoothly, assisting in setting up, cleaning, and finally, closing the party.

## 4. Transport Layer
The Transport Layer ensures end-to-end communication between devices. It is responsible for flow control, error correction, and retransmission of lost data. Common transport layer protocols include TCP (Transmission Control Protocol) and UDP (User Datagram Protocol).

## 5. Network Layer
The Network Layer focuses on the logical addressing and routing of data between devices in different networks. It enables devices to communicate across multiple networks and chooses the best path for data transmission. A key protocol operating at this layer is IP (Internet Protocol).

## 6. Data Link Layer
The Data Link Layer is responsible for creating a reliable link between two directly connected nodes. It frames data into frames, adds MAC (Media Access Control) addresses, and performs error detection. Examples of protocols operating at this layer include Ethernet and PPP (Point-to-Point Protocol).

## 7. Physical Layer
The Physical Layer deals with the physical connection between devices. It defines characteristics such as voltage levels, cable types, and data rates. Hardware components like cables, connectors, and network interface cards operate at this layer. The primary goal is to transmit raw binary data over a physical medium.


## Reference
https://www.youtube.com/watch?v=vv4y_uOneC0

https://www.geeksforgeeks.org/open-systems-interconnection-model-osi/